# vodqareactnative-mobile-automation

### Project aims
The project developed to cover VodQaReactNative android mobile app with functional tests using BDD approach

## Prerequisites
### Local setup
#### Install Appium using Node.js
* The first step is to verify whether Node.js is already installed in the system:
    ```comandline
    node --version
    ```
If Node.js is already installed in the system, it’ll return the Node version, else it throws an error stating – `node is not recognized`, which means Node.js is not installed in the system
* To install Appium, run the following command:
    ```comandline
    npm install -g appium@1.22.0
    ```
The command above will download and install Appium. Once done, verify the Appium installation on macOS or Windows by running the command below:
   ```comandline
    appium --version
   ```
This will return the corresponding Appium version installed in your system
* Before proceeding with any test scenarios, consider installing and running Appium doctor. It is a tool that identifies and resolves common configuration issues before Appium automation.
Install Appium Doctor using the following command:
    ```comandline
    npm install appium-doctor -g
    ```
Once the tool is installed, one can view the list of inspections available by running the command below:
   ```comandline
    appium-doctor -h
   ```
#### Install Android Studio, Android SDK Tools
* Download [Android studio](https://developer.android.com/studio) (including Virtual Device Manager) and following instructions for installation
* Install [Android SDK](https://developer.android.com/tools) - set up Android SDK Build Tools, Android SDK Platform Tools, Android SDK Android Emulator
* Create Android virtual device using Virtual Device Manager and start it on your system
Once the device is up, one can view the list of available devices connected by running the command below:
    ```comandline
    adb devices
    ```
This will return the corresponding virtual or real devices connected to the system
```comandline
    List of devices attached
    emulator-5554   device
```
`emulator-5554` here is the UDID of virtual device which could be attached to `appium.udid` property value to be able run tests using virtual or real device locally

[PLEASE NOTE]
* To avoid configuration issue before Appium automation execution - `JAVA_HOME`, `ANDROID_HOME`, `MAVEN_HOME` should be defined as System Variables
* Locations of `java_jdk`, `m2_home`, `nodejs`, `sdk_tools`, `sdk_platform-tools` and `sdk_emulator` should be added to `Path` System Variable
* Current framework supports programmatic starting of local Appium Server instance using parameters determined in `appium.properties`

### BrowserStack Cloud setup

* Create new FREE TRIAL account on [Browserstack](https://www.browserstack.com/users/sign_up)
* Extract `BROWSERSTACK_USERNAME` and `BROWSERSTACK_ACCESS_KEY`
* Upload Android application ander test to the Browserstack platform using one of the [available methods](https://www.browserstack.com/docs/app-automate/appium/upload-app-from-filesystem)
* Extract generated `app_url` value that should be attached to `browserstack.app` property value (e.g. `bs://f7c874f21852ba57957a3fdc33f47514288c4ba4`)

## Base workflow
1. Checkout project from GIT
    ```commandline
    git clone https://gitlab.com/qatestchallenge/mobile-test-automation.git
    ```
2. [OPTIONAL] Then you need to connect  **mobile-test-automation**:
    1. Open `mobile-test-automation/android` project in IDE
    2. Click on `Maven Projects` bar
    3. Click on `Add Maven Project`
    4. Go to `mobile-test-automation/android` folder and select `pom.xml file`
    5. Click OK
    6. Navigate to Project Setting-> Build-> Build Tools-> Maven-> Choose maven version 3.6.0
    7. Navigate to Project Setting-> Build-> Compiler-> Java Compiler-> Choose Java 8

### Technical description
- [Java 8](https://openjdk.org/projects/jdk8/)
- [IntelliJ Idea](https://www.jetbrains.com/idea/) highly recommended IDE
- [Maven 3](https://maven.apache.org/guides/) - project management tool
- [Appium](https://appium.io/) - mobile app testing framework
- [JUnit 4](https://junit.org/junit4/) - test engine
- [Cucumber](https://cucumber.io/docs/guides/) - BDD tool
- [git](https://git-scm.com/) - source code management tool
- [BrowserStack](https://www.browserstack.com/docs/) Cloud solution for test execution


## Running

### Local using CLI
Run tests with command line parameters:
   ```commandline
   git clone https://gitlab.com/qatestchallenge/mobile-test-automation.git
   cd mobile-test-automation/android
   mvn clean test -Dcucumber.filter.tags=@regression -Dappium.remote=false -Dappium.udid="emulator-5554" -Dappium.deviceName="Pixel_2_API_28" -Dappium.platformVersion="9.0"
   ```
### BrowserStack Cloud using CLI
Run tests with command line parameters:
```commandline
    git clone https://gitlab.com/qatestchallenge/mobile-test-automation.git
    cd mobile-test-automation/android
    mvn clean test -Dcucumber.filter.tags=@regression -Dappium.remote=true -Dappium.deviceName="Google Pixel 2" -Dappium.platformVersion="9.0"
   ```

### Maven usage
You can `set any properties` via Maven
Description of maven options (cmd line args):
* `clean` - clean up all temporary data from previous run
* `test` - execute tests
* `-Dappium.udid="emulator-5554"` - maven parameter (java style). Explanation:
    * `-D` - indicate that it java system property
    * `appium.udid` - property name
    * `emulator-5554` - property value. Please use double quotes if value has spaces `"value with spaces"`

### Appium properties
| Property | Default value                              | remark                                    |
| --- |--------------------------------------------|-------------------------------------------|
|appium.remote | false                                      | local or remote (BrowserStack) execution  |
|appium.local.host | 127.0.0.1                                  | local Appium Hub default localhost        |
|appium.local.port | 4723                                       | local Appium Hub default port             |
|appium.local.path | /wd/hub                                    | local Appium Hub base path                |
|appium.deviceName | Pixel_2_API_28                             | emulated or real device name              |
|appium.platformName | Android                                    | mobile platform name                      |
|appium.platformVersion | 9.0                                        | mobile platform version                   |
|appium.automationName | UIAutomator2                               | Appium driver name                        |
|appium.udid| emulator-5554                              | emulated or real device unique identifier |
|appium.app| src/main/resources/app/appiumChallenge.apk | path to app under test location           |
|appium.newCommandTimeout | 60                                         | Appium new command timeout in seconds     |
|appium.noReset | false                               | Appium no reset device value              |

### Android properties
| Property | Default value                            | remark                                                                     |
| --- |------------------------------------------|----------------------------------------------------------------------------|
|android.appPackage | com.vodqareactnative                                    | app under test package value                                               |
|android.appActivity | com.vodqareactnative.MainActivity                                   | app under test activity value                                              |
|android.autoGrantPermissions | true                                    | automatically determine which permissions your app requires and grant them |
|android.orientation| PORTRAIT                                | device screen orientation                                                  |
[PLEASE NOTE] If `appium.noReset` capability is set to true, `android.autoGrantPermissions` capability doesn’t work.

### BrowserStack properties
| Property                   | Default value                                  | remark                                                   |
|----------------------------|------------------------------------------------|----------------------------------------------------------|
| browserstack.username | BROWSERSTACK_USERNAME                          | BrowserStack username                                    |
| browserstack.apikey | BROWSERSTACK_API_KEY                           | BrowserStack api key                                     |
| browserstack.url | http://%s:%s@hub-cloud.browserstack.com/wd/hub | remote Appium Hub host                                   |
| browserstack.app  | bs://a00717e109da8db8bd0b13f28275d916cc53ba9c  | specify app under test                                   |
| browserstack.projectName   | vodqareactnative-mobile-automation             | unique identifier to group multiple builds               |
| browserstack.buildName     | 1.0-SNAPSHOT | unique identifier to group multiple test sessions        |
| browserstack.appiumVersion | 1.22.0  | Appium version                                           |
| browserstack.local         | false      | test localhost / internal servers in your network        |
| browserstack.idleTimeout   | 60            | timeout value in seconds                                 |
| browserstack.debug         | true        | enable automatic screenshots for various Appium commands |
| browserstack.networkLogs   | true        | enable network logs                                      |

### Cucumber properties
| Property | Default value                             | remark                                     |
| --- |-------------------------------------------|--------------------------------------------|
|cucumber.publish.enabled | true                                      | enable publishing of test results          |
|cucumber.publish.quiet | true                                      | suppress publish banner after test execution |
|cucumber.execution.dry-run | false                                     | tests dry-run option                       |
|cucumber.snippet-type | camelcase                                 | snippet-type value                         |
|cucumber.plugin | html:target/cucumber-reports.html,pretty  | comma separated plugin strings             |
|cucumber.filter.tags | @regression                               | cucumber tag expression                    |

## Reporting

### Cucumber Reporting

* Cucumber report generates as HTML based on values of `cucumber.plugin=html:target/cucumber-reports.html` and `cucumber.publish.enabled=true` property
* Generated report location is `target/cucumber-reports.html`
* Cucumber report Overview screen for generated report show general results per test execution:  
![cucumber_report_general_view](img/cucumber_report_gen_view.png)
* Cucumber report Passed filter view per feature:  
![cucumber_report_passed_expanded](img/cucumber_report_passed_exp.png)
* Cucumber report Failed test cases filter per feature with attached screenshot:  
![cucumber_report_failed_expanded](img/cucumber_report_failed_tc.png)

### BrowserStack Dashboard
* BrowserStack Dashboard view with all available sessions:  
![browserstack_dashboard_sessions_list](img/bs_dashboard_sessions.png)
* BrowserStack particular session detailed view including - session tedails, session video recording, text logs with steps visualization and log levels filtering, networks logs, app profiling, etc.:  
![browserstack_dashboard_passed_session](img/bs_session_view.png)
* Here you can find public link of test session build - [BrowserStack test session build example](https://app-automate.browserstack.com/builds/70d7edc279ac844436ffd254148eb57ccccee9fc/sessions/83c162564240b0421b158333ba1a6ef8e70b2284?auth_token=2e8079a309551c41f926aac3309d4f5bd5f4d28caf581f8b3f256b91549c88ed)
