Feature: Validate VodQaReactNative android app login

  @regression
  @smoke
  Scenario Outline: Login app as valid user
    When <user> enter credential and submit
    Then user successfully logged in app
    Examples:
      | user |
      | "validUser" |

  @regression
  Scenario Outline: Login app as invalid user
    When <user> enter credential and submit
    Then user get alert with error information
    Examples:
      | user |
      | "invalidUser1" |
      | "invalidUser2" |