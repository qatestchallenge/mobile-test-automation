Feature: Validate VodQaReactNative android app Long Press gesture

  Background:
    Given default user logged in app

  @regression
  @smoke
  Scenario: Long press target button
    Given Long Press widget opened
    When user perform Long press gesture
    Then user get alert Long Press success information