Feature: Validate VodQaReactNative android app Double Tap gesture

  Background:
    Given default user logged in app

  @regression
  @smoke
  Scenario: Double tap target button
    Given Double Tab widget opened
    When user perform Double tap gesture
    Then user get alert Double Tap success information