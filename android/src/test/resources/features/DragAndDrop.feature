Feature: Validate VodQaReactNative android app Drag & Drop gesture

  Background:
    Given default user logged in app

  @regression
  @smoke
  Scenario: Drag circle to the Drop zone
    Given Drag and Drop widget opened
    When user drag circle to the Drop zone by gesture
    Then "Circle dropped" message displayed