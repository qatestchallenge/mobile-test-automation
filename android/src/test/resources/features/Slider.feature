Feature: Validate VodQaReactNative android app Slider gesture

  Background:
    Given default user logged in app

    @regression
    @smoke
    Scenario Outline: Drag slider point to the right
      Given Slider widget opened
      When user drag slider to the right by <percentage>
      Then slider position changed
      Examples:
      | percentage |
      | "50%" |

