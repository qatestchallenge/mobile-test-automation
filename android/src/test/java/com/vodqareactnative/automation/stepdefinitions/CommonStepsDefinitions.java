package com.vodqareactnative.automation.stepdefinitions;

import com.vodqareactnative.automation.data.User;
import com.vodqareactnative.automation.screens.LoginScreen;
import com.vodqareactnative.automation.utils.JsonObjectReaderUtils;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;

import static com.vodqareactnative.automation.data.StaticData.USERS_JSON_PATH;

public class CommonStepsDefinitions {

    @When("{string} enter credential and submit")
    public void enterValidCredentialAndSubmit(String username) {
        User user = JsonObjectReaderUtils.createObject(User.class, USERS_JSON_PATH, username);
        new LoginScreen().login(user);
    }

    @Given("default user logged in app")
    public void defaultUserLoggedInApp() {
        new LoginScreen().submitLogin();
    }

}
