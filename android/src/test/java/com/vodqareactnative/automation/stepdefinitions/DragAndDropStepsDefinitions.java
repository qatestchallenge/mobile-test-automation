package com.vodqareactnative.automation.stepdefinitions;

import com.vodqareactnative.automation.screens.DragAndDropScreen;
import com.vodqareactnative.automation.screens.SamplesListScreen;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;

public class DragAndDropStepsDefinitions {
    private DragAndDropScreen dragAndDropScreen;

    @Given("Drag and Drop widget opened")
    public void dragAndDropWidgetOpened() {
        dragAndDropScreen = new SamplesListScreen().openDragAndDropWidget();
    }

    @When("user drag circle to the Drop zone by gesture")
    public void userDragCircleToTheDropZoneByGesture() {
        dragAndDropScreen.dragAndDropCircleElementToDropZone();
    }

    @Then("{string} message displayed")
    public void messageDisplayed(String expectedText) {
        Assertions.assertThat(dragAndDropScreen.getSuccessDragAndDropMessage())
                .withFailMessage("Circle doesn't dragged to Drop zone")
                .isEqualTo(expectedText);
    }

}
