package com.vodqareactnative.automation.stepdefinitions;

import com.vodqareactnative.automation.data.expected.SliderExpectedResults;
import com.vodqareactnative.automation.screens.SamplesListScreen;
import com.vodqareactnative.automation.screens.SliderScreen;
import com.vodqareactnative.automation.utils.JsonObjectReaderUtils;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;

import static com.vodqareactnative.automation.data.StaticData.SLIDER_JSON_PATH;

public class SliderStepDefinitions {
    private SliderScreen sliderScreen;
    private SliderExpectedResults sliderExpectedResults;

    @Given("Slider widget opened")
    public void sliderWidgetOpened() {
        sliderScreen = new SamplesListScreen().openSliderWidget();
    }

    @When("user drag slider to the right by {string}")
    public void userMoveSliderToTheMiddle(String percentage) {
        sliderExpectedResults = JsonObjectReaderUtils.createObject(SliderExpectedResults.class, SLIDER_JSON_PATH, percentage);
        sliderScreen.dragSliderRightByPercentage(sliderExpectedResults.getDragRightPercentage());
    }

    @Then("slider position changed")
    public void sliderPositionIsOnTheMiddle() {
        Assertions.assertThat(sliderScreen.getSliderPositionText())
                .withFailMessage("Slider point doesn't dragged out")
                .isEqualTo(sliderExpectedResults.getTargetSliderPositionText());
    }

}
