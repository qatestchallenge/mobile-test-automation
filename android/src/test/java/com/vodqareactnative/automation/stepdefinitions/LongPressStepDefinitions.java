package com.vodqareactnative.automation.stepdefinitions;

import com.vodqareactnative.automation.screens.LongPressScreen;
import com.vodqareactnative.automation.screens.SamplesListScreen;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;

public class LongPressStepDefinitions {
    private LongPressScreen longPressScreen;

    @Given("Long Press widget opened")
    public void longPressWidgetOpened() {
        longPressScreen = new SamplesListScreen().openLongPressWidget();
    }

    @When("user perform Long press gesture")
    public void userPerformLongPressGesture() {
        longPressScreen.longPressButton();
    }

    @Then("user get alert Long Press success information")
    public void userGetAlertLongPressSuccessInformation() {
        Assertions.assertThat(longPressScreen.isSuccessLongPressPopUpDisplayed())
                .withFailMessage("Long press doesn't occurred")
                .isTrue();
    }

}
