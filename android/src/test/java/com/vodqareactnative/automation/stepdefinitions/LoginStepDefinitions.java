package com.vodqareactnative.automation.stepdefinitions;

import com.vodqareactnative.automation.screens.BaseScreen;
import com.vodqareactnative.automation.screens.LoginScreen;
import com.vodqareactnative.automation.screens.SamplesListScreen;
import io.cucumber.java.en.Then;

import static org.assertj.core.api.Assertions.assertThat;

public class LoginStepDefinitions extends BaseScreen {
    @Then("user successfully logged in app")
    public void userSuccessfullyLoggedInApp() {
        assertThat(new SamplesListScreen().getSamplesListScreenTitle())
                .withFailMessage("Login is unsuccessful, Samples List screen isn't loaded")
                .isEqualTo("Samples List");
    }

    @Then("user get alert with error information")
    public void userGetPopupWithErrorInformation() {
        assertThat(new LoginScreen().isInvalidLoginPopUpDisplayed())
                .withFailMessage("Login invalid login popup not displayed")
                .isTrue();
    }

}
