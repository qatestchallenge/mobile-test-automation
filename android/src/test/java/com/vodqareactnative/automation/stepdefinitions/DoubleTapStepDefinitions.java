package com.vodqareactnative.automation.stepdefinitions;

import com.vodqareactnative.automation.screens.DoubleTapScreen;
import com.vodqareactnative.automation.screens.SamplesListScreen;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;

public class DoubleTapStepDefinitions {
    private DoubleTapScreen doubleTapScreen;

    @Given("Double Tab widget opened")
    public void doubleTabWidgetOpened() {
        doubleTapScreen = new SamplesListScreen().openDoubleTapWidget();
    }

    @When("user perform Double tap gesture")
    public void userPerformDoubleTabGesture() {
        doubleTapScreen.doubleTapButton();
    }

    @Then("user get alert Double Tap success information")
    public void userGetAlertSuccessInformation() {
        Assertions.assertThat(doubleTapScreen.isSuccessDoubleTapPopUpDisplayed())
                .withFailMessage("Double tab doesn't occurred")
                .isTrue();
    }

}
