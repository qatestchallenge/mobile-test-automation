package com.vodqareactnative.automation.hooks;

import com.vodqareactnative.automation.context.AppiumConfig;
import com.vodqareactnative.automation.context.DriverManager;
import com.vodqareactnative.automation.context.ServerManager;
import io.appium.java_client.MobileCommand;
import io.cucumber.java.*;
import org.openqa.selenium.OutputType;

public class Hooks {

    @BeforeAll
    public static void setUp() {
        if (!AppiumConfig.isRemote()) {
            ServerManager.startAppiumServer();
        }
        DriverManager.startAppiumDriver();
    }

    @Before
    public void startApp() {
        DriverManager.getDriver().execute(MobileCommand.LAUNCH_APP);
    }

    @After
    public void attachScreenshot(Scenario scenario) {
        if(scenario.isFailed()){
            byte[] screenshot = DriverManager.getDriver().getScreenshotAs(OutputType.BYTES);
            scenario.attach(screenshot, "image/png", scenario.getName());
        }
    }

    @After
    public void terminateApp() {
        DriverManager.getDriver().execute(MobileCommand.CLOSE_APP);
    }

    @AfterAll
    public static void tearDown() {
        DriverManager.getDriver().quit();
        if (!AppiumConfig.isRemote()) {
            ServerManager.getService().stop();
        }
        DriverManager.removeDriver();
    }

}
