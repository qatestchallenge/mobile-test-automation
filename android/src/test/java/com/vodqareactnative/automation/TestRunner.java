package com.vodqareactnative.automation;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features",
        glue = {"com/vodqareactnative/automation/stepdefinitions", "com/vodqareactnative/automation/hooks"})
public class TestRunner {
}
