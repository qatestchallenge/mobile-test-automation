package com.vodqareactnative.automation.screens;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

public class DragAndDropScreen extends BaseScreen {
    @AndroidFindBy(accessibility = "dropzone")
    private WebElement dropzone;
    @AndroidFindBy(accessibility = "dragMe")
    private WebElement dragElement;
    @AndroidFindBy(accessibility = "success")
    private WebElement dragSuccessElement;

    public DragAndDropScreen() {
        super();
    }

    public DragAndDropScreen dragAndDropCircleElementToDropZone() {
        dragAndDropGesture(dragElement, dropzone);
        return this;
    }

    public String getSuccessDragAndDropMessage() {
        return dragSuccessElement.getText().trim();
    }

}
