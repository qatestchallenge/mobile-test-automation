package com.vodqareactnative.automation.screens;

import com.vodqareactnative.automation.context.DriverManager;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Pause;
import org.openqa.selenium.interactions.PointerInput;
import org.openqa.selenium.interactions.Sequence;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

import static com.vodqareactnative.automation.utils.WebElementsUtils.getElementCenterX;
import static com.vodqareactnative.automation.utils.WebElementsUtils.getElementCenterY;
import static java.time.Duration.ofMillis;
import static java.util.Collections.singletonList;
import static org.openqa.selenium.interactions.PointerInput.MouseButton.LEFT;
import static org.openqa.selenium.interactions.PointerInput.MouseButton.MIDDLE;
import static org.openqa.selenium.interactions.PointerInput.Origin.viewport;

public class BaseScreen {
    private static final PointerInput FINGER = new PointerInput(PointerInput.Kind.TOUCH, "finger");
    private static final int INITIAL_LENGTH = 1;
    private static final Duration ZERO_DURATION = ofMillis(0);

    public BaseScreen() {
        PageFactory.initElements(new AppiumFieldDecorator(DriverManager.getDriver()), this);
    }

    protected void singleTapGesture(WebElement source) {
        DriverManager.getDriver().perform(singletonList(getTapGestureSequence(source, ofMillis(500))));
    }

    protected void horizontalDragGesture(WebElement webElement, int percentage) {
        Point source = webElement.getLocation();
        int targetX = webElement.getSize().getWidth() * percentage / 100;
        DriverManager.getDriver().perform(singletonList(getDragGestureSequence(source, targetX)));
    }

    protected void dragAndDropGesture(WebElement source, WebElement target) {
        DriverManager.getDriver().perform(singletonList(getDragAndDropGestureSequence(source, target)));
    }

    protected void doubleTapGesture(WebElement webElement) {
        DriverManager.getDriver().perform(singletonList(getDoubleTapGestureSequence(webElement, ofMillis(200))));
    }

    protected void longPressGesture(WebElement webElement) {
        DriverManager.getDriver().perform(singletonList(getTapGestureSequence(webElement, ofMillis(3000))));
    }

    private Sequence getTapGestureSequence(WebElement source, Duration duration) {
        return new Sequence(FINGER, INITIAL_LENGTH)
                .addAction(FINGER.createPointerMove(ZERO_DURATION, viewport(), getElementCenterX(source), getElementCenterY(source)))
                .addAction(FINGER.createPointerDown(LEFT.asArg()))
                .addAction(new Pause(FINGER, duration))
                .addAction(FINGER.createPointerUp(LEFT.asArg()));
    }

    private Sequence getDragGestureSequence(Point source, int targetX) {
        return new Sequence(FINGER, INITIAL_LENGTH)
                .addAction(FINGER.createPointerMove(ZERO_DURATION, viewport(), source.x, source.y))
                .addAction(FINGER.createPointerDown(MIDDLE.asArg()))
                .addAction(new Pause(FINGER, ofMillis(1000)))
                .addAction(FINGER.createPointerMove(ofMillis(1000), viewport(), source.x + targetX, source.y))
                .addAction(FINGER.createPointerUp(MIDDLE.asArg()));
    }

    private Sequence getDragAndDropGestureSequence(WebElement source, WebElement target) {
        return new Sequence(FINGER, INITIAL_LENGTH)
                .addAction(FINGER.createPointerMove(ZERO_DURATION, viewport(), getElementCenterX(source), getElementCenterY(source)))
                .addAction(FINGER.createPointerDown(LEFT.asArg()))
                .addAction(new Pause(FINGER, ofMillis(500)))
                .addAction(FINGER.createPointerMove(ofMillis(2000), viewport(), getElementCenterX(target), getElementCenterY(target)))
                .addAction(FINGER.createPointerUp(LEFT.asArg()));
    }

    private Sequence getDoubleTapGestureSequence(WebElement source, Duration duration) {
        return new Sequence(FINGER, INITIAL_LENGTH)
                .addAction(FINGER.createPointerMove(ZERO_DURATION, viewport(), getElementCenterX(source), getElementCenterY(source)))
                .addAction(FINGER.createPointerDown(LEFT.asArg()))
                .addAction(FINGER.createPointerUp(LEFT.asArg()))
                .addAction(new Pause(FINGER, duration))
                .addAction(FINGER.createPointerDown(LEFT.asArg()))
                .addAction(FINGER.createPointerUp(LEFT.asArg()));
    }

}
