package com.vodqareactnative.automation.screens;

import com.vodqareactnative.automation.data.User;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

public class LoginScreen extends BaseScreen {
    @AndroidFindBy(accessibility = "username")
    private WebElement usernameField;
    @AndroidFindBy(accessibility = "password")
    private WebElement passwordField;
    @AndroidFindBy(accessibility = "login")
    private WebElement loginButton;
    @AndroidFindBy(id = "android:id/message")
    private WebElement popupMessage;
    @AndroidFindBy(id = "android:id/button1")
    private WebElement popupOkButton;

    public LoginScreen() {
        super();
    }

    public SamplesListScreen login(User user) {
        enterUsername(user.getUsername())
                .enterPassword(user.getPassword())
                .submitLogin();
        return new SamplesListScreen();
    }

    private LoginScreen enterUsername(String username) {
        usernameField.clear();
        usernameField.sendKeys(username);
        return this;
    }

    private LoginScreen enterPassword(String password) {
        passwordField.clear();
        passwordField.sendKeys(password);
        return this;
    }

    public LoginScreen submitLogin() {
        singleTapGesture(loginButton);
        return this;
    }

    public boolean isInvalidLoginPopUpDisplayed() {
        boolean result;
        result = "Invalid  Credentials".equals(popupMessage.getText());
        singleTapGesture(popupOkButton);
        return result;
    }

}
