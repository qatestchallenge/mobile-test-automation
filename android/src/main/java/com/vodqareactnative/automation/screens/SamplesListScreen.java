package com.vodqareactnative.automation.screens;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

public class SamplesListScreen extends BaseScreen {
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.widget.TextView")
    private WebElement samplesTitle;
    @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc=\"slider1\"]")
    private WebElement slider;
    @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc=\"dragAndDrop\"]")
    private WebElement dragAndDrop;
    @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc=\"doubleTap\"]")
    private WebElement doubleTap;
    @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc=\"longPress\"]")
    private WebElement longPress;

    public SamplesListScreen() {
        super();
    }

    public String getSamplesListScreenTitle() {
        return samplesTitle.getText().trim();
    }

    public SliderScreen openSliderWidget() {
        singleTapGesture(slider);
        return new SliderScreen();
    }

    public DragAndDropScreen openDragAndDropWidget() {
        singleTapGesture(dragAndDrop);
        return new DragAndDropScreen();
    }

    public DoubleTapScreen openDoubleTapWidget() {
        singleTapGesture(doubleTap);
        return new DoubleTapScreen();
    }

    public LongPressScreen openLongPressWidget() {
        singleTapGesture(longPress);
        return new LongPressScreen();
    }

}
