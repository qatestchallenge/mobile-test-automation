package com.vodqareactnative.automation.screens;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

public class SliderScreen extends BaseScreen {
    @AndroidFindBy(accessibility = "slider")
    private WebElement slider;

    public SliderScreen() {
        super();
    }

    public SliderScreen dragSliderRightByPercentage(int percentage) {
        horizontalDragGesture(slider, percentage);
        return this;
    }

    public String getSliderPositionText() {
        return slider.getText().trim();
    }

}
