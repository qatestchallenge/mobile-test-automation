package com.vodqareactnative.automation.screens;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

public class LongPressScreen extends BaseScreen {
    @AndroidFindBy(accessibility = "longpress")
    private WebElement longPress;
    @AndroidFindBy(id = "android:id/message")
    private WebElement popupMessage;
    @AndroidFindBy(id = "android:id/button1")
    private WebElement popupOkButton;

    public LongPressScreen() {
        super();
    }

    public LongPressScreen longPressButton() {
        longPressGesture(longPress);
        return this;
    }

    public boolean isSuccessLongPressPopUpDisplayed() {
        boolean result;
        result = "you pressed me hard :P".equals(popupMessage.getText());
        singleTapGesture(popupOkButton);
        return result;
    }

}
