package com.vodqareactnative.automation.screens;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

public class DoubleTapScreen extends BaseScreen {
    @AndroidFindBy(accessibility = "doubleTapMe")
    private WebElement doubleTapMeButton;
    @AndroidFindBy(id = "android:id/message")
    private WebElement popupMessage;
    @AndroidFindBy(id = "android:id/button1")
    private WebElement popupOkButton;

    public DoubleTapScreen() {
        super();
    }

    public DoubleTapScreen doubleTapButton() {
        doubleTapGesture(doubleTapMeButton);
        return this;
    }

    public boolean isSuccessDoubleTapPopUpDisplayed() {
        boolean result;
        result = "Double tap successful!".equals(popupMessage.getText());
        singleTapGesture(popupOkButton);
        return result;
    }

}
