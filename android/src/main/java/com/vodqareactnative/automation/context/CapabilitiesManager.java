package com.vodqareactnative.automation.context;

import io.appium.java_client.android.options.UiAutomator2Options;
import io.appium.java_client.remote.MobileCapabilityType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.time.Duration;
import java.util.HashMap;

import static com.vodqareactnative.automation.context.AndroidConfig.*;
import static com.vodqareactnative.automation.context.AppiumConfig.*;
import static com.vodqareactnative.automation.context.BrowserstackConfig.*;

public class CapabilitiesManager {
    public static final Logger LOGGER = LogManager.getLogger(CapabilitiesManager.class);

    public UiAutomator2Options getUiAutomator2Options() {
        UiAutomator2Options options = new UiAutomator2Options();
        options.setDeviceName(getDeviceName());
        options.setPlatformName(getPlatformName());
        options.setPlatformVersion(getPlatformVersion());
        options.setNewCommandTimeout(Duration.ofSeconds(getNewCommandTimeout()));
        options.setNoReset(isNoReset());
        options.setAppPackage(getAppPackage());
        options.setAppActivity(getAppActivity());
        options.setAutoGrantPermissions(isAutoGrantPermissions());
        options.setOrientation(ScreenOrientation.valueOf(getOrientation()));
        options.setUdid(getUdid());
        options.setAutomationName(getAutomationName());
        options.setApp(new File(getApp()).getAbsolutePath());
        LOGGER.info(String.format("Local Device options for %s %s -> %s device is ready:\n%s",
                getPlatformName(), getPlatformVersion(), getDeviceName(), options.toJson()));
        return options;
    }

    public DesiredCapabilities getRemoteDesiredCapabilities() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        HashMap<String, Object> browserstackOptions = new HashMap<>();
        browserstackOptions.put("projectName", getBrowserstackProjectName());
        browserstackOptions.put("buildName", getBrowserstackBuildName());
        browserstackOptions.put("appiumVersion", getBrowserstackAppiumVersion());
        browserstackOptions.put("local", isBrowserstackLocal());
        browserstackOptions.put("idleTimeout", getBrowserstackIdleTimeout());
        browserstackOptions.put("debug", isBrowserstackDebug());
        browserstackOptions.put("networkLogs", isBrowserstackNetworkLogs());
        capabilities.setCapability("bstack:options", browserstackOptions);
        capabilities.setCapability(CapabilityType.PLATFORM_NAME, getPlatformName());
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, getPlatformVersion());
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, getDeviceName());
        capabilities.setCapability(MobileCapabilityType.APP, getBrowserstackAppUrl());
        LOGGER.info(String.format("Remote Device capabilities for %s %s -> %s device is ready:\n%s",
                getPlatformName(), getPlatformVersion(), getDeviceName(), capabilities.toJson()));
        return capabilities;
    }

}
