package com.vodqareactnative.automation.context;

import java.util.ResourceBundle;

public class AndroidConfig {
    private static final ResourceBundle resourceBundle = ResourceBundle.getBundle("android");
    private static final String APP_PACKAGE = "android.appPackage";
    private static final String APP_ACTIVITY = "android.appActivity";
    private static final String AUTO_GRANT_PERMISSIONS = "android.autoGrantPermissions";
    private static final String ORIENTATION = "android.orientation";

    private AndroidConfig() {
    }

    public static String getAppPackage() {
        return System.getProperty(APP_PACKAGE, resourceBundle.getString(APP_PACKAGE));
    }

    public static String getAppActivity() {
        return System.getProperty(APP_ACTIVITY, resourceBundle.getString(APP_ACTIVITY));
    }

    public static boolean isAutoGrantPermissions() {
        return Boolean.parseBoolean(System.getProperty(AUTO_GRANT_PERMISSIONS, resourceBundle.getString(AUTO_GRANT_PERMISSIONS)));
    }

    public static String getOrientation() {
        return System.getProperty(ORIENTATION, resourceBundle.getString(ORIENTATION));
    }

}
