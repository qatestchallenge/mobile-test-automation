package com.vodqareactnative.automation.context;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServerHasNotBeenStartedLocallyException;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;

public class ServerManager {
    private static final Logger LOGGER = LogManager.getLogger(ServerManager.class);
    private static final String WIN_NODE_EXE_PATH = "C:" + File.separator + "Program Files (x86)" + File.separator +
            "nodejs" + File.separator + "node.exe";
    public static final String WIN_APPIUM_JS_PATH = "C:" + File.separator + "Users" + File.separator + System.getProperty("user.name") +
            File.separator + "AppData" + File.separator + "Roaming" + File.separator + "npm" + File.separator +
            "node_modules" + File.separator + "appium" + File.separator + "build" + File.separator + "lib" + File.separator + "main.js";
    private static ThreadLocal<AppiumDriverLocalService> service = new ThreadLocal<>();

    private ServerManager() {
    }

    public static AppiumDriverLocalService getService() {
        return service.get();
    }

    public static void startAppiumServer() {
        LOGGER.info("Starting to initialize Appium Local Server...");
        AppiumDriverLocalService localService = AppiumDriverLocalService.buildService(getAppiumServiceBuilder());
        localService.start();
        if (null == localService || !localService.isRunning()) {
            throw new AppiumServerHasNotBeenStartedLocallyException("Appium Local Server not started");
        }
        localService.clearOutPutStreams();
        service.set(localService);
        LOGGER.info("Appium Local Server successfully started");
    }

    private static AppiumServiceBuilder getAppiumServiceBuilder() {
        String osName = System.getProperty("os.name");
        AppiumServiceBuilder appiumServiceBuilder;
        if (osName.toLowerCase().contains("win")) {
            appiumServiceBuilder = getWindowsAppiumServerBuilder();
        } else {
            throw new IllegalArgumentException(String.format("No such OS %s", osName));
        }
        return appiumServiceBuilder;
    }

    private static AppiumServiceBuilder getWindowsAppiumServerBuilder() {
        return new AppiumServiceBuilder()
                .usingDriverExecutable(new File(WIN_NODE_EXE_PATH))
                .withAppiumJS(new File(WIN_APPIUM_JS_PATH))
                .withIPAddress(AppiumConfig.getLocalHubHost())
                .usingPort(AppiumConfig.getLocalHubPort())
                .withArgument(GeneralServerFlag.BASEPATH, AppiumConfig.getLocalHubPath())
                .withArgument(GeneralServerFlag.SESSION_OVERRIDE)
                .withArgument(GeneralServerFlag.RELAXED_SECURITY)
                .withLogFile(new File("server.log"));
    }

}
