package com.vodqareactnative.automation.context;

import java.util.ResourceBundle;

public class AppiumConfig {
    private static final ResourceBundle resourceBundle = ResourceBundle.getBundle("appium");
    private static final String REMOTE = "appium.remote";
    private static final String LOCAL_HUB_HOST = "appium.local.host";
    private static final String LOCAL_HUB_PORT = "appium.local.port";
    private static final String LOCAL_HUB_PATH = "appium.local.path";
    private static final String DEVICE_NAME = "appium.deviceName";
    private static final String PLATFORM_NAME = "appium.platformName";
    private static final String PLATFORM_VERSION = "appium.platformVersion";
    private static final String AUTOMATION_NAME = "appium.automationName";
    private static final String UDID = "appium.udid";
    private static final String NEW_COMMAND_TIMEOUT = "appium.newCommandTimeout";
    private static final String APP = "appium.app";
    private static final String NO_RESET = "appium.noReset";

    private AppiumConfig() {
    }

    public static boolean isRemote() {
        return Boolean.parseBoolean(System.getProperty(REMOTE, resourceBundle.getString(REMOTE)));
    }

    public static String getLocalHubHost() {
        return System.getProperty(LOCAL_HUB_HOST, resourceBundle.getString(LOCAL_HUB_HOST));
    }

    public static int getLocalHubPort() {
        return Integer.parseInt(System.getProperty(LOCAL_HUB_PORT, resourceBundle.getString(LOCAL_HUB_PORT)));
    }

    public static String getLocalHubPath() {
        return System.getProperty(LOCAL_HUB_PATH, resourceBundle.getString(LOCAL_HUB_PATH));
    }

    public static String getDeviceName() {
        return System.getProperty(DEVICE_NAME, resourceBundle.getString(DEVICE_NAME));
    }

    public static String getPlatformName() {
        return System.getProperty(PLATFORM_NAME, resourceBundle.getString(PLATFORM_NAME));
    }

    public static String getPlatformVersion() {
        return System.getProperty(PLATFORM_VERSION, resourceBundle.getString(PLATFORM_VERSION));
    }

    public static String getAutomationName() {
        return System.getProperty(AUTOMATION_NAME, resourceBundle.getString(AUTOMATION_NAME));
    }

    public static String getUdid() {
        return System.getProperty(UDID, resourceBundle.getString(UDID));
    }

    public static int getNewCommandTimeout() {
        return Integer.parseInt(System.getProperty(NEW_COMMAND_TIMEOUT, resourceBundle.getString(NEW_COMMAND_TIMEOUT)));
    }

    public static String getApp() {
        return System.getProperty(APP, resourceBundle.getString(APP));
    }

    public static boolean isNoReset() {
        return Boolean.parseBoolean(System.getProperty(NO_RESET, resourceBundle.getString(NO_RESET)));
    }

}
