package com.vodqareactnative.automation.context;

import java.util.ResourceBundle;

public class BrowserstackConfig {
    private static final ResourceBundle resourceBundle = ResourceBundle.getBundle("browserstack");
    public static final String BROWSERSTACK_HUB_URL = "browserstack.url";
    public static final String BROWSERSTACK_USERNAME = "browserstack.username";
    public static final String BROWSERSTACK_API_KEY = "browserstack.apikey";
    public static final String BROWSERSTACK_APP_URL = "browserstack.app";
    public static final String BROWSERSTACK_PROJECT_NAME = "browserstack.projectName";
    public static final String BROWSERSTACK_BUILD_NAME = "browserstack.buildName";
    public static final String BROWSERSTACK_APPIUM_VERSION = "browserstack.appiumVersion";
    public static final String BROWSERSTACK_LOCAL = "browserstack.local";
    public static final String BROWSERSTACK_IDLE_TIMEOUT = "browserstack.idleTimeout";
    public static final String BROWSERSTACK_DEBUG = "browserstack.debug";
    public static final String BROWSERSTACK_NETWORK_LOGS = "browserstack.networkLogs";

    private BrowserstackConfig() {
    }

    public static String getBrowserstackHubUrl() {
        return System.getProperty(BROWSERSTACK_HUB_URL, resourceBundle.getString(BROWSERSTACK_HUB_URL));
    }

    public static String getBrowserstackUsername() {
        return System.getProperty(BROWSERSTACK_USERNAME, resourceBundle.getString(BROWSERSTACK_USERNAME));
    }

    public static String getBrowserstackApiKey() {
        return System.getProperty(BROWSERSTACK_API_KEY, resourceBundle.getString(BROWSERSTACK_API_KEY));
    }

    public static String getBrowserstackAppUrl() {
        return System.getProperty(BROWSERSTACK_APP_URL, resourceBundle.getString(BROWSERSTACK_APP_URL));
    }

    public static String getBrowserstackProjectName() {
        return System.getProperty(BROWSERSTACK_PROJECT_NAME, resourceBundle.getString(BROWSERSTACK_PROJECT_NAME));
    }

    public static String getBrowserstackBuildName() {
        return System.getProperty(BROWSERSTACK_BUILD_NAME, resourceBundle.getString(BROWSERSTACK_BUILD_NAME));
    }

    public static String getBrowserstackAppiumVersion() {
        return System.getProperty(BROWSERSTACK_APPIUM_VERSION, resourceBundle.getString(BROWSERSTACK_APPIUM_VERSION));
    }

    public static Boolean isBrowserstackLocal() {
        return Boolean.valueOf(System.getProperty(BROWSERSTACK_LOCAL, resourceBundle.getString(BROWSERSTACK_LOCAL)));
    }

    public static int getBrowserstackIdleTimeout() {
        return Integer.parseInt(System.getProperty(BROWSERSTACK_IDLE_TIMEOUT, resourceBundle.getString(BROWSERSTACK_IDLE_TIMEOUT)));
    }

    public static Boolean isBrowserstackDebug() {
        return Boolean.valueOf(System.getProperty(BROWSERSTACK_DEBUG, resourceBundle.getString(BROWSERSTACK_DEBUG)));
    }

    public static Boolean isBrowserstackNetworkLogs() {
        return Boolean.valueOf(System.getProperty(BROWSERSTACK_NETWORK_LOGS, resourceBundle.getString(BROWSERSTACK_NETWORK_LOGS)));
    }

}
