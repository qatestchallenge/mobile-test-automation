package com.vodqareactnative.automation.context;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.MalformedURLException;
import java.net.URL;

import static com.vodqareactnative.automation.context.BrowserstackConfig.*;

public class DriverManager {
    private static final Logger LOGGER = LogManager.getLogger(DriverManager.class);
    private static ThreadLocal<AppiumDriver> primaryDriver = new ThreadLocal<>();

    private DriverManager() {
    }

    public static AppiumDriver getDriver() {
        return primaryDriver.get();
    }

    public static void setDriver(AppiumDriver appiumDriver) {
        primaryDriver.set(appiumDriver);
    }

    public static void removeDriver() {
        primaryDriver.remove();
    }

    public static void startAppiumDriver() {
        AndroidDriver androidDriver = null;
        CapabilitiesManager capabilitiesManager = new CapabilitiesManager();
        LOGGER.info("Starting to initialize Appium Driver instance...");
        if (AppiumConfig.isRemote()) {
            try {
                androidDriver = new AndroidDriver(new URL(String.format(getBrowserstackHubUrl(),
                        getBrowserstackUsername(), getBrowserstackApiKey())), capabilitiesManager.getRemoteDesiredCapabilities());
            } catch (MalformedURLException e) {
                LOGGER.error("Could not start Remote Driver instance");
            }
        } else {
            if ("android".equalsIgnoreCase(AppiumConfig.getPlatformName())) {
                androidDriver = new AndroidDriver(ServerManager.getService().getUrl(), capabilitiesManager.getUiAutomator2Options());
            } else {
                throw new IllegalArgumentException(String.format("No such platform %s", AppiumConfig.getPlatformName()));
            }
        }
        LOGGER.info("Appium Driver successfully started");
        primaryDriver.set(androidDriver);
    }

}
