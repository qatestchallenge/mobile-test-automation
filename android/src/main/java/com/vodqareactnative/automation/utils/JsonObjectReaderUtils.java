package com.vodqareactnative.automation.utils;

import io.cucumber.core.internal.com.fasterxml.jackson.core.JsonProcessingException;
import io.cucumber.core.internal.com.fasterxml.jackson.databind.JsonNode;
import io.cucumber.core.internal.com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;

public class JsonObjectReaderUtils {
    private static final Logger LOGGER = LogManager.getLogger(JsonObjectReaderUtils.class);

    private JsonObjectReaderUtils() {
    }

    public static <T> T createObject(Class<T> valueType, String filePath, String section) {
        return createObject(valueType, getJsonNode(filePath, section));
    }

    private static <T> T createObject(Class<T> valueType, JsonNode jsonNode) {
        String content = jsonNode.toString();
        Object object;
        try {
            object = getObjectMapper().readValue(content, valueType);
            return valueType.cast(object);
        } catch (IOException e) {
            LOGGER.error("Could not read value from json to an object", e);
        } catch (ClassCastException e) {
            LOGGER.error("Could not cast to " + valueType.getCanonicalName(), e);
        }
        return null;
    }

    private static JsonNode getJsonNode(File file, String section) {
        JsonNode internalJsonNode = null;
        try {
            internalJsonNode = getObjectMapper().readTree(file).path(section);
        } catch (JsonProcessingException e) {
            LOGGER.error("Problem in parsing Json file: " + file.getPath(), e);
        } catch (IOException e) {
            LOGGER.error("File is not found, path to file: " + file.getPath(), e);
        }
        return internalJsonNode;
    }

    private static ObjectMapper getObjectMapper() {
        return new ObjectMapper();
    }

    private static JsonNode getJsonNode(String filePath, String section) {
        return getJsonNode(new File(filePath), section);
    }

}
