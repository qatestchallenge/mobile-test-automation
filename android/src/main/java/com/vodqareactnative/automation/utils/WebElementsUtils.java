package com.vodqareactnative.automation.utils;

import org.openqa.selenium.WebElement;

public class WebElementsUtils {

    private WebElementsUtils() {
    }

    public static int getElementCenterY(WebElement dragElement) {
        return dragElement.getLocation().getY() + dragElement.getSize().getHeight() / 2;
    }

    public static int getElementCenterX(WebElement dragElement) {
        return dragElement.getLocation().getX() + dragElement.getSize().getWidth() / 2;
    }

}
