package com.vodqareactnative.automation.data;

public class StaticData {
    public static final String USERS_JSON_PATH = "src/main/resources/data/users.json";
    public static final String SLIDER_JSON_PATH = "src/main/resources/data/expected/slider.json";

    private StaticData() {
    }

}
