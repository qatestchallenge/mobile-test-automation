package com.vodqareactnative.automation.data.expected;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SliderExpectedResults {
    private int dragRightPercentage;
    private int dragLeftPercentage;
    private String initSliderPositionText;
    private String targetSliderPositionText;

}
